// power_heart
// Author: Brad Cable
// License: MIT

#include "config.h"

#ifndef _LEDOBJECT
#define _LEDOBJECT

struct LEDState {
	uint16_t num_leds;
	CRGB* leds;
};

class LEDObject {
	protected:
		struct LEDState ledState;

	public:
		LEDObject(uint16_t num_leds);
		~LEDObject();

		struct LEDState getState();
		virtual void tick();
};

#endif
