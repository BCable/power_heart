// power_heart
// Author: Brad Cable
// License: MIT

#include "config.h"
#include "ledobject.h"

#ifndef _POWERBAR
#define _POWERBAR

class PowerBar: public LEDObject {
	private:
		uint16_t plevel_actual;
		uint16_t plevel_lag;

	public:
		PowerBar();
		uint16_t getPower();
		void setState();
		void suckPower();
		void tick();
};

#endif
