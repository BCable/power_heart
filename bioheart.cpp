// power_heart
// Author: Brad Cable
// License: MIT

#include "bioheart.h"

BioHeart::BioHeart(PowerBar* pBar): LEDObject(BIOHEART_COUNT){
	this->beat_state = 0;
	this->pBar = pBar;

	// LED initialization
	for(uint8_t i=0; i<BIOHEART_COUNT; i++){
		this->ledState.leds[i] = 0x400000;
	}
}

void BioHeart::beat(){
	this->pBar->suckPower();
}

void BioHeart::tick(){
	// initial beat
	if(this->beat_state == 1 || this->beat_state == 11){
		this->beat();
		for(uint8_t i=0; i<BIOHEART_COUNT; i++){
			this->ledState.leds[i] = 0x200000;
		}
	}

	// standard beating
	else if(
		(this->beat_state > 1 && this->beat_state < 7) ||
		(this->beat_state > 11 && this->beat_state < 17)
	){
		for(uint8_t i=0; i<BIOHEART_COUNT; i++){
			if(i%2 == this->beat_state%2){
				this->ledState.leds[i] = 0x040001;
			} else {
				this->ledState.leds[i] = 0x100000;
			}
		}

	// standard not-beating
	} else {
		for(uint8_t i=0; i<BIOHEART_COUNT; i++){
			this->ledState.leds[i] = 0x020000;
		}
	}

	this->beat_state++;
	this->beat_state %= 48;
}
