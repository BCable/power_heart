power_heart 1.0.0
=================

This is simple FastLED usage for NeoPixel LEDs for an Arduino project.  A biological creature's heart sucks power out of a power source that is continuously charging, and the power source is directly tied to lightbulbs.  Pictures later.
