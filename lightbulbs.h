// power_heart
// Author: Brad Cable
// License: MIT

#include "config.h"
#include "ledobject.h"
#include "powerbar.h"

#ifndef _LIGHTBULBS
#define _LIGHTBULBS

#define LIGHT_OFF 0
#define LIGHT_ON 1
#define LIGHT_FLICKER 2

class LightBulbs: public LEDObject {

	private:
		PowerBar* pBar;
		uint8_t lights_state[LIGHTBULBS_COUNT];

	public:
		LightBulbs(PowerBar* pBar);
		uint8_t getLightsOff();
		uint8_t getLightsOn();
		uint8_t getLightsFlicker();
		void setRandomLightOn();
		void setRandomLightFlicker();
		void setLightFlicker(uint8_t light_no);
		void setLightOff(uint8_t light_no);
		void setLightOn(uint8_t light_no);
		void tick();
};

#endif
