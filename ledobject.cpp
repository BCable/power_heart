// power_heart
// Author: Brad Cable
// License: MIT

#include "ledobject.h"
#include "stdlib.h"

LEDObject::LEDObject(uint16_t num_leds){
	this->ledState.num_leds = num_leds;
	this->ledState.leds = (CRGB*) malloc(
		sizeof(CRGB) * this->ledState.num_leds
	);
}

LEDObject::~LEDObject(){
	free(this->ledState.leds);
}

struct LEDState LEDObject::getState(){
	return this->ledState;
}
