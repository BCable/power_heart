// power_heart
// Author: Brad Cable
// License: MIT

#include "FastLED.h"

// debug mode
//#define DEBUG

// FastLED config
#define NUM_LEDS 300
#define DATA_PIN 3
//#define CLOCK_PIN 13
#define RANDOMSEED_PIN 0
#define BRIGHTNESS 120

// power_bioheart config
#define BIOHEART_COUNT 7
#define LIGHTBULBS_COUNT 6
#define LIGHTBULBS_WIDTH 3
#define POWERBAR_COUNT 20

#define BIOHEART_BEATSPEED 5
#define LIGHTBULB_POWERPAD 0
#define POWERBAR_MAXPOWER 255
#define POWERBAR_CHARGEAMT 2
#define POWERBAR_DRAINAMT 2
#define POWERBAR_SUCKAMTMAX 80
#define POWERBAR_SUCKAMTMIN 15
