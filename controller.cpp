// power_heart
// Author: Brad Cable
// License: MIT

#include "controller.h"

Controller::Controller(CRGB* leds): LEDObject(
	BIOHEART_COUNT + (LIGHTBULBS_COUNT*LIGHTBULBS_WIDTH) + POWERBAR_COUNT
){
	uint16_t i;

	this->leds = leds;

	// create logical objects
	this->pBar = new PowerBar();
	this->bHeart = new BioHeart(this->pBar);
	this->lBulbs = new LightBulbs(this->pBar);

	// reset all LEDs on strip
	for(i=0; i<NUM_LEDS; i++){
		this->leds[i] = CRGB::Black;
	}
}

void Controller::mergeState(){
	uint16_t i, pos = 0;

	struct LEDState bHeartState = this->bHeart->getState();
	struct LEDState lBulbsState = this->lBulbs->getState();
	struct LEDState pBarState = this->pBar->getState();

	for(i=0; i<BIOHEART_COUNT; i++){
		this->ledState.leds[pos] = bHeartState.leds[i];
		pos++;
	}

	for(i=0; i<LIGHTBULBS_COUNT*LIGHTBULBS_WIDTH; i++){
		this->ledState.leds[pos] = lBulbsState.leds[i];
		pos++;
	}

	for(i=0; i<POWERBAR_COUNT; i++){
		this->ledState.leds[pos] = pBarState.leds[i];
		pos++;
	}

	for(i=0; i<this->ledState.num_leds; i++){
		this->leds[i] = this->ledState.leds[i];
	}
}

void Controller::tick(){
	this->bHeart->tick();
	this->lBulbs->tick();
	this->pBar->tick();
}
