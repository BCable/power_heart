// power_heart
// Author: Brad Cable
// License: MIT

#include "FastLED.h"

#include "config.h"
#include "controller.h"

CRGB leds[NUM_LEDS];
Controller* control;

void setup(){
	// starting setup
	randomSeed(analogRead(RANDOMSEED_PIN));
	control = new Controller(leds);
	FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
	FastLED.setBrightness(BRIGHTNESS);

	// DEBUG setup
#ifdef DEBUG
	Serial.begin(115200);
#endif
}

void loop(){
	control->mergeState();
	FastLED.show();
	delay(50);
	control->tick();
}
