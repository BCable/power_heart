// power_heart
// Author: Brad Cable
// License: MIT

#include "powerbar.h"

PowerBar::PowerBar(): LEDObject(POWERBAR_COUNT){
	this->plevel_actual = POWERBAR_MAXPOWER;
	this->plevel_lag = POWERBAR_MAXPOWER;

	// LED initialization
	for(uint8_t i=0; i<POWERBAR_COUNT; i++){
		this->ledState.leds[i] = 0x101010;
	}
}

uint16_t PowerBar::getPower(){
	return this->plevel_actual;
}

void PowerBar::suckPower(){
	uint8_t suck_amount = random(POWERBAR_SUCKAMTMIN, POWERBAR_SUCKAMTMAX);

	if(suck_amount > this->plevel_actual){
		this->plevel_actual = 0;
	} else {
		this->plevel_actual -= suck_amount;
	}
}

void PowerBar::setState(){
	float lights_on;
	float lights_faded;
	uint8_t i;

	lights_on = this->plevel_actual;
	lights_on /= POWERBAR_MAXPOWER;
	lights_on *= POWERBAR_COUNT;

	lights_faded = this->plevel_lag;
	lights_faded /= POWERBAR_MAXPOWER;
	lights_faded *= POWERBAR_COUNT;

	if(this->plevel_actual == POWERBAR_MAXPOWER){
		for(i=0; i<POWERBAR_COUNT; i++){
			this->ledState.leds[i] = 0x001000;
		}

	} else {
		for(i=0; i<POWERBAR_COUNT; i++){
			if(i <= (uint8_t) lights_on){
				this->ledState.leds[i] = 0x101010;

			} else if(i < (uint8_t) lights_faded){
				this->ledState.leds[i] = 0x040200;

			} else if(i == lights_faded){
				this->ledState.leds[i] = 0x040000;

			} else {
				this->ledState.leds[i] = 0x000000;
			}
		}
	}
}

void PowerBar::tick(){
	if(POWERBAR_DRAINAMT > this->plevel_lag){
		this->plevel_lag = 0;
	} else {
		this->plevel_lag -= POWERBAR_DRAINAMT;
	}

	if(this->plevel_actual + POWERBAR_CHARGEAMT > POWERBAR_MAXPOWER){
		this->plevel_actual = POWERBAR_MAXPOWER;
	} else {
		this->plevel_actual += POWERBAR_CHARGEAMT;
		if(this->plevel_actual > this->plevel_lag){
			this->plevel_lag = this->plevel_actual;
		}
	}

	this->setState();
}
