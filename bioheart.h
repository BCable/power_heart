// power_heart
// Author: Brad Cable
// License: MIT

#include "config.h"
#include "ledobject.h"
#include "powerbar.h"

#ifndef _BIOHEART
#define _BIOHEART

class BioHeart: public LEDObject {

	private:
		uint8_t beat_state;
		PowerBar* pBar;

	public:
		BioHeart(PowerBar* pBar);
		void beat();
		void tick();

};

#endif
