// power_heart
// Author: Brad Cable
// License: MIT

#include "config.h"

#include "bioheart.h"
#include "ledobject.h"
#include "lightbulbs.h"
#include "powerbar.h"

class Controller: public LEDObject {

	private:
		CRGB* leds;

		BioHeart* bHeart;
		LightBulbs* lBulbs;
		PowerBar* pBar;

	public:
		Controller(CRGB* leds);
		void mergeState();
		void tick();
};
