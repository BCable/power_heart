// power_heart
// Author: Brad Cable
// License: MIT

#include "lightbulbs.h"

LightBulbs::LightBulbs(PowerBar* pBar): LEDObject(LIGHTBULBS_COUNT){
	this->pBar = pBar;

	// power on lights
	for(uint8_t i=0; i<LIGHTBULBS_COUNT; i++){
		this->lights_state[i] = LIGHT_ON;
	}

	// LED initialization
	for(uint8_t i=0; i<BIOHEART_COUNT; i++){
		this->ledState.leds[i] = 0x101000;
	}
}

uint8_t LightBulbs::getLightsOff(){
	uint8_t ret = 0;
	for(uint8_t i=0; i<LIGHTBULBS_COUNT; i++){
		if(this->lights_state[i] == LIGHT_OFF){
			ret++;
		}
	}
	return ret;
}

uint8_t LightBulbs::getLightsOn(){
	uint8_t ret = 0;
	for(uint8_t i=0; i<LIGHTBULBS_COUNT; i++){
		if(this->lights_state[i] == LIGHT_ON){
			ret++;
		}
	}
	return ret;
}

uint8_t LightBulbs::getLightsFlicker(){
	uint8_t ret = 0;
	for(uint8_t i=0; i<LIGHTBULBS_COUNT; i++){
		if(this->lights_state[i] >= LIGHT_FLICKER){
			ret++;
		}
	}
	return ret;
}

void LightBulbs::setLightFlicker(uint8_t light_no){
	this->lights_state[light_no] = LIGHT_FLICKER;
}

void LightBulbs::setLightOff(uint8_t light_no){
	this->lights_state[light_no] = LIGHT_OFF;
}

void LightBulbs::setLightOn(uint8_t light_no){
	this->lights_state[light_no] = LIGHT_ON;
}

void LightBulbs::setRandomLightOn(){
	uint8_t i, pos, random_light;

	random_light = random(0, this->getLightsOff() + this->getLightsFlicker());
	pos=0;
	for(i=0; i<LIGHTBULBS_COUNT; i++){
		if(this->lights_state[i] != LIGHT_ON){
			if(pos == random_light){
				this->setLightOn(i);
				break;
			}
			pos++;
		}
	}
}

void LightBulbs::setRandomLightFlicker(){
	uint8_t i, pos, random_light;

	random_light = random(0, this->getLightsOn());
	pos=0;
	for(i=0; i<LIGHTBULBS_COUNT; i++){
		if(this->lights_state[i] == LIGHT_ON){
			if(pos == random_light){
				this->setLightFlicker(pos);
				break;
			}
			pos++;
		}
	}
}

void LightBulbs::tick(){
	uint16_t power_level = this->pBar->getPower();

	if(power_level > POWERBAR_MAXPOWER-LIGHTBULB_POWERPAD){
		power_level = POWERBAR_MAXPOWER;
	} else {
		power_level += LIGHTBULB_POWERPAD;
	}

	float power_pct = ((float) power_level) / POWERBAR_MAXPOWER;

	if(power_pct > 1){
		power_pct == 1;
	}

	uint8_t lights_powered = this->getLightsOn();
	uint8_t lights_nopower = this->getLightsOff() + this->getLightsFlicker();
	uint8_t lights_topower = power_pct * LIGHTBULBS_COUNT;
	uint8_t i, j, pos;

#ifdef DEBUG
	Serial.print("this->pBar->getPower(): ");
	Serial.println(this->pBar->getPower());
	Serial.print("power_pct: ");
	Serial.println(power_pct);
	Serial.print("lights_powered: ");
	Serial.println(lights_powered);
	Serial.print("lights_nopower: ");
	Serial.println(lights_nopower);
	Serial.print("lights_topower: ");
	Serial.println(lights_topower);
#endif

	// set lights to be turned back on
	if(lights_topower > lights_powered){
		for(i=0; i<lights_topower-lights_powered; i++){
			this->setRandomLightOn();
#ifdef DEBUG
			Serial.println("POWER ON LED");
#endif
		}

	// set lights to be flickering
	} else {
		for(i=0; i<lights_powered-lights_topower; i++){
			this->setRandomLightFlicker();
		}
	}

	// do flicker animations and LED on/off
#ifdef DEBUG
	Serial.print("STATE: ");
#endif
	pos = 0;
	for(i=0; i<LIGHTBULBS_COUNT; i++){
		for(j=0; j<LIGHTBULBS_WIDTH; j++){
			if(this->lights_state[i] == LIGHT_OFF){
				this->ledState.leds[pos] = 0x000000;
#ifdef DEBUG
				Serial.print(" 0");
#endif

			} else if(this->lights_state[i] == LIGHT_ON){
				this->ledState.leds[pos] = 0x101000;
#ifdef DEBUG
				Serial.print(" 1");
#endif

			} else {
#ifdef DEBUG
				Serial.print(" .");
#endif

				if(this->lights_state[i]%3 == 0){
					this->ledState.leds[pos] = 0x040400;
				} else if(this->lights_state[i]%3 == 1){
					this->ledState.leds[pos] = 0x101000;
				} else {
					this->ledState.leds[pos] = 0x020200;
				}

				// iterate animation and shut off light if necessary
				if(j+1 == LIGHTBULBS_WIDTH){
					this->lights_state[i] = (this->lights_state[i]+1)%16;
					if(this->lights_state[i] == LIGHT_OFF){
						this->setLightOff(i);
					}
				}
			}
			pos++;
		}
	}
#ifdef DEBUG
	Serial.println("");
#endif
}
